from django.urls import re_path
from bio import views

urlpatterns = [
    re_path(r'^bio/$', views.bio),
]
