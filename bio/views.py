from django.shortcuts import render
from home.models import Project


def bio(request):

    context = {'page': 'bio',
               'menu': 'studio',
               'years': [i.date for i in Project.date.get_queryset()]}

    return render(request, 'bio/bio.html', context=context)
