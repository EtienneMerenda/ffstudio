from django.urls import re_path
from contact import views

urlpatterns = [
    re_path(r'^contact/$', views.contact),
    re_path(r'^contact/mail/',
            views.mail),
]
