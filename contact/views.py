from django.shortcuts import render
from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import redirect
from home.models import Project

from pprint import pprint


def contact(request):

    context = {'page': 'contact',
               'menu': 'contact',
               'years': [i.date for i in Project.date.get_queryset()]}

    return render(request, 'contact/contact.html', context)


def mail(request):

    post = request.POST

    # Verify if is bot
    if post["g-recaptcha-response"] == "":
        return render(request, 'contact/contact.html', context={'recaptcha': True})

    else:

        texte = f'De: {post["name"]}\n\nMail: {post["email"]}\n\n' + post['text']

        object = "Message issue du site FFStudio"

        res = send_mail(object,
                        texte,
                        "ffstudio@vivaldi.net",
                        ["info@ffstudio.fr"])

        return render(request, 'contact/contact.html', context={'mail': True})
