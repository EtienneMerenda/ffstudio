from django.contrib import admin
from django.contrib.admin import AdminSite
from .models import (Image, Category, Date, Project,
                     DesignFeature, ArchitectureFeature)


class MyAdminSite(AdminSite):
    site_header = "FFstudio"
    site_title = "Site d'administration FFStudio"

    def get_app_list(self, request):
        """
        Return a sorted list of all the installed apps that have been
        registered in this site.
        """
        ordering = {
            "Projet": 1,
            "Catégories": 2,
            "Années": 3,
            "Images": 4,
            "Information du projet de design": 5,
            "Information du projet d'architecture": 6,

            "Publications": 1,
            "Informations": 2,
            "Liens": 3,
        }
        app_dict = self._build_app_dict(request)
        # a.sort(key=lambda x: b.index(x[0]))
        # Sort the apps alphabetically.
        app_list = sorted(app_dict.values(), key=lambda x: x['name'].lower())

        # Sort the models alphabetically within each app.
        for app in app_list:
            app['models'].sort(key=lambda x: ordering[x['name']])

        return app_list


class PictureAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'image_display', 'miniature_display')
    list_filter = ('project',)
    search_fields = ('image_title',)
    readonly_fields = ('image_display', 'miniature_display')


class Sorted(admin.ModelAdmin):
    ordering = ['date']


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('__str__',
                    'number',
                    'category',
                    'date',
                    'fisrt_miniature_display')

    list_filter = ('name', 'category', 'date')

    ordering = ['category', '-date', 'number']

    fieldsets = (
        (None, {
            'fields': (('name', 'number'),
                       ('category', 'date'),
                       'text',
                       ('designFeature',), #desginFeature),
                       ('architectureFeature',),)
        }),
        ('Image associée', {
            'classes': ('collapse',),
            'fields': ('all_image',),
        }),
    )

    readonly_fields = ('first_image_display',
                       'fisrt_miniature_display',
                       'all_image')


myAdminSite = MyAdminSite(name="HomeAdminSite")

myAdminSite.register(Image, PictureAdmin)
#myAdminSite.register(Category)
myAdminSite.register(Date, Sorted)
myAdminSite.register(Project, ProjectAdmin)
myAdminSite.register(DesignFeature)
myAdminSite.register(ArchitectureFeature)
