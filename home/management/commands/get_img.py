from django.core.management.base import BaseCommand
from django.core.files import File
from home.models import Image, Category, Date, Project, DesignFeature, ArchitectureFeature
import os
import re
from pprint import pprint
import random


class Command(BaseCommand):
    args = "<à quoi sers tu ?>"
    help = "Fonction qui scanne les fichiers dans media photos et les ajoute "\
           "dans la base de donnée."

    def _get_image_info(self):

        img_list = []

        path_work = "./media/pictures"

        # Get information of pictures in pictures folder

        # scan category lvl
        for category_ in [i for i in os.listdir(path_work) if i in ["Architecture", "Design"]]:
            if os.path.isdir(path_work+"/"+category_):
                cat_path = path_work+"/"+category_
                # year lvl
                for year_ in os.listdir(cat_path):
                    year_path = cat_path + "/" + year_
                    if os.path.isdir(year_path):
                        # project lvl
                        for project_ in os.listdir(year_path):
                            project_path = year_path + "/" + project_
                            # Picture lvl
                            for picture_ in os.listdir(project_path):
                                if picture_[2:5] == "min":
                                    pass
                                else:
                                    info = {}
                                    pic_path = project_path + "/" + picture_
                                    minPicPath = project_path + "/" + picture_[0:1] + "_min" + picture_[1:]
                                    info["projectNumber"] = project_[0:2]
                                    info["projectName"] = project_[3:].replace("_", " ")
                                    info["projectCategory"] = category_
                                    info["projectDate"] = year_
                                    info["imageNumber"] = picture_[0]
                                    print(picture_[0], picture_)
                                    info["imageMiniaturePath"] = re.sub("./static", "", minPicPath)
                                    info["imagePath"] = re.sub("./static", "", pic_path)
                                    pprint(info)

                                    if info['imagePath'] not in [i['imagePath'] for i in img_list]:
                                        img_list.append(info)

            else:
                print("end")

        lorem = ['Ut tempus odio eget nibh blandit dignissim.',
                 'Suspendisse vel ante nec diam elementum euismod eu a lorem.',
                 'Sed et ex ac massa',
                 'Integer',
                 'in maximus mollis.',
                 'Donec porta elit ut sapien malesuada, quis faucibus odio suscipit.',
                 'sit',
                 'ultricies diam nec, vestibulum.'
                 ]

        quickLorem = ["Lorem",
                      "maximus mollis.",
                      "ac massa",
                      "Sed et ex ac massa"]

        Image.objects.all().delete()
        Project.objects.all().delete()
        Date.objects.all().delete()
        Category.objects.all().delete()
        DesignFeature.objects.all().delete()
        ArchitectureFeature.objects.all().delete()

        i = 0
        for img in img_list:
            i += 1
            print(i)
            if img["imagePath"] not in [i.image.url for i in Image.objects.all()]:

                image_ = Image()
                image_.number = int(img["imageNumber"])

                image_.caption = lorem[random.randrange(0, 8)]
                image_.credit = f'credit à {lorem[random.randrange(0, 8)]}'
                # Adding some lorem Ipsum for fill rows in project table

                try:
                    print(img["projectName"])
                    image_.project = Project.objects.get(name=img["projectName"])
                except:
                    if img["projectCategory"] == "Architecture":
                        date, created = Date.objects.get_or_create(date=img["projectDate"])
                        cat, created = Category.objects.get_or_create(category=img["projectCategory"])
                        archiFeature, created = ArchitectureFeature.objects.get_or_create(date=quickLorem[random.randrange(0, 4)],
                                                                                      mission=quickLorem[random.randrange(0, 4)],
                                                                                      size=quickLorem[random.randrange(0, 4)],
                                                                                      statut=quickLorem[random.randrange(0, 4)],)

                        image_.project, created = Project.objects.get_or_create(name=img["projectName"],
                                                                                number=int(img["projectNumber"]),
                                                                                text=lorem[random.randrange(0, 8)],
                                                                                category=cat,
                                                                                date=date,
                                                                                architectureFeature=archiFeature)
                    else:
                        date, created = Date.objects.get_or_create(date=img["projectDate"])
                        cat, created = Category.objects.get_or_create(category=img["projectCategory"])
                        desFeature, created = DesignFeature.objects.get_or_create(date=quickLorem[random.randrange(0, 4)],
                                                                          measure=quickLorem[random.randrange(0, 4)],
                                                                          material=quickLorem[random.randrange(0, 4)])
                        image_.project, created = Project.objects.get_or_create(name=img["projectName"],
                                                                                number=int(img["projectNumber"]),
                                                                                text=lorem[random.randrange(0, 8)],
                                                                                category=cat,
                                                                                date=date,
                                                                                designFeature=desFeature)

                with open(img["imagePath"], 'rb') as f:
                    image_file = File(f)
                    image_.image = image_file
                    with open(img["imageMiniaturePath"], 'rb') as f_:
                        image_file_ = File(f_)
                        image_.miniature = image_file_

                        image_.save()
                print(f'image n°{img["projectNumber"]} \'{img["projectName"]}\' added in db.')

    def handle(self, *args, **options):
        self._get_image_info()
