# Generated by Django 2.2.2 on 2019-08-06 07:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_auto_20190731_1847'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='image',
            options={'ordering': ['project', 'number'], 'verbose_name': '4. Image'},
        ),
    ]
