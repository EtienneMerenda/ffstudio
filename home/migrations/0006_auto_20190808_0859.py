# Generated by Django 2.2.2 on 2019-08-08 06:59

from django.db import migrations, models
import home.models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0005_auto_20190807_1826'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='image',
            field=models.ImageField(unique=True, upload_to=home.models.Image.project_path, verbose_name="Ajout d'image (H: 1500px L: 2250px)"),
        ),
    ]
