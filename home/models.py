from django.db import models
from django.utils.safestring import mark_safe
from django.utils.html import format_html, format_html_join, escape
import re

class Category(models.Model):

    # Architect / Design
    category = models.CharField(max_length=15, unique=True, verbose_name='Catégorie du projet')

    def __str__(self):
        return self.category

    class Meta:
        verbose_name = 'Catégorie'
        ordering = ['category']


class DesignFeature(models.Model):
    # Design
    measure = models.CharField(max_length=150, blank=True,
                               null=True, verbose_name='Dimensions')
    date = models.CharField(max_length=150, blank=True,
                            null=True, verbose_name='Date de création')
    material = models.CharField(max_length=150, blank=True,
                                null=True, verbose_name='Matériaux')

    def __str__(self):
        return f"Données design n°{self.id}: {self.measure} / {self.date} / {self.material}"

    class Meta:
        verbose_name = 'Information du projet de design'
        verbose_name_plural = 'Information du projet de design'


class ArchitectureFeature(models.Model):
    # Architecture
    date = models.CharField(max_length=150, blank=True,
                            null=True, verbose_name='Date')
    mission = models.CharField(max_length=150, blank=True,
                               null=True, verbose_name='Mission')
    size = models.CharField(max_length=150, blank=True,
                            null=True, verbose_name='Superficie')
    statut = models.CharField(max_length=150, blank=True,
                              null=True, verbose_name='Statut')

    def __str__(self):
        return f"Données architecture n°{self.id}: {self.date} / {self.mission} / {self.size} / {self.statut}"

    class Meta:
        verbose_name = 'Information du projet d\'architecture'
        verbose_name_plural = 'Information du projet d\'architecture'



class Date(models.Model):
    # 2017 / 2018 / 2019
    date = models.SmallIntegerField(unique=True, verbose_name='Année du projet')

    def __str__(self):
        return str(self.date)

    class Meta:
        verbose_name = 'Année'
        ordering = ['date']


class Project(models.Model):
    # Manage all project display in FFstudio website
    name = models.CharField(max_length=150, unique=True, verbose_name='Nom du projet')
    # Number for order.
    number = models.SmallIntegerField(blank=True, verbose_name='Numéro du projet')
    text = models.TextField(blank=True, verbose_name='Description du projet')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Catégorie du projet')
    date = models.ForeignKey(Date, on_delete=models.CASCADE, verbose_name='Année du projet')
    designFeature = models.ForeignKey(DesignFeature, blank=True, null=True, on_delete=models.CASCADE, verbose_name='Données Design')
    architectureFeature = models.ForeignKey(ArchitectureFeature, blank=True, null=True, on_delete=models.CASCADE, verbose_name='Données Architecture')

    def first_image_display(self):
        # Return first image of image field for admin
        try:
            img = format_html('<img src="{}" style="width: 200px; height: auto;">',
                              mark_safe(self.image_set.all()[0].image.url))
            return img

        except ValueError:
            noImg = format_html('<span>{}</span>',
                                'Aucune image.')
            return noImg

    first_image_display.short_description = "Image"

    def fisrt_miniature_display(self):
        # Return first thumbnail of image field for admin
        try:
            img = format_html('<img src="{}" style="width: 100px; height: auto;">',
                              mark_safe(self.image_set.all()[0].miniature.url))
            return img

        except (ValueError, IndexError):
            noImg = format_html('<span>{}</span>',
                                'Aucune image.')
            return noImg

    fisrt_miniature_display.short_description = "Miniature"

    def all_image(self):
        # Return all thumbnails to display then in django admin
        try:

            url = [(i.miniature.url,) for i in self.image_set.all()]

            img = format_html_join('\n', '<img src="{}" style="width:'
                                   ' 200px; height: auto;">',
                                   url,)

            return img

        except (ValueError, IndexError):
            noImg = format_html('<span>{}</span>',
                                'Aucune image.')
            return noImg

    all_image.short_description = 'Image'

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Projet'
        verbose_name_plural = 'Projet'
        ordering = ['number']


# model used for store url of pictures and descritpion
class Image(models.Model):
    """Table used for get path of some pictures"""

    # return th path for save the file
    def project_path(instance, filename):

        if re.search('min', filename):
            path = f'pictures/Project/{instance.project.name}/{instance.number}_min.jpg'.replace(' ', '_')
        else:
            path = f'pictures/Project/{instance.project.name}/{instance.number}.jpg'.replace(' ', '_')

        return path

    image = models.ImageField(upload_to=project_path, unique=True, verbose_name='Ajout d\'image (H: 1500px L: 2250px)')

    # Return image for admin gestion
    def image_display(self):

        try:
            img = format_html('<img src="{}" style="width: 200px; height: auto;">',
                              mark_safe(self.image.url))
            return img

        except ValueError:
            noImg = format_html('<span>{}</span>',
                                'Aucune image.')
            return noImg

    image_display.short_description = "Image"

    miniature = models.ImageField(upload_to=project_path, unique=True, verbose_name='Ajout de la miniature (H: 310px L: 342px)')

    def miniature_display(self):
        try:
            img = format_html('<img src="{}" style="width: 100px; height: auto;">',
                              mark_safe(self.miniature.url))
            return img

        except ValueError:
            noImg = format_html('<span>{}</span>',
                                'Aucune image.')
            return noImg

    miniature_display.short_description = "Miniature"

    number = models.SmallIntegerField(verbose_name=('Numéro de la photo'))
    alt = models.CharField(max_length=200, blank=True, verbose_name='Alt tag')

    # info under picture.
    caption = models.CharField(max_length=100, blank=True, verbose_name='Résumé du projet')
    credit = models.CharField(max_length=100, blank=True, verbose_name='Auteur de la photo')

    # ForeignKey
    project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name='Projet')

    def __str__(self):
        return f"Image n°{self.number} du projet {self.project}."

    class Meta:
        verbose_name = 'Image'
        ordering = ['project', 'number']
