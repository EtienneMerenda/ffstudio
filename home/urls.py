from django.urls import re_path
from home import views

urlpatterns = [
    re_path(r'^$', views.home),
    re_path(r'^(?P<category>[A-Z][a-z]+)/$', views.home_selected),
    re_path(r'^(?P<category>[A-Z][a-z]+)/(?P<date>[0-9]+)/$', views.home_selected),
    re_path(r'^ajax/project/$', views.get_project),
    re_path(r'^ajax/category/date/$', views.menu_request),
]
