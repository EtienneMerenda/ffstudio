from django.shortcuts import render
from django.http import JsonResponse
from django.shortcuts import (get_object_or_404,
                              get_list_or_404)
from django.template import RequestContext

from .models import Project
import random
from pprint import pprint

altList = ["Architecture", "Architecte", "Architectecte D'interieur",
           "Décoration", "Aménagement", "Agencement", "Renovation",
           "Extension", "Mobilier Design", "Travaux", "Gros Œuvre",
           "Chantier", "Permis de construire", "Déclaration Préalable",
           "Autorisation Mairie", "Construire", "Construire Maison",
           "Maison", "Paysagisme", "Home-Staging", "Compagnon du devoir",
           "artisan", "Particulier", "Pro", "Chateau", "Demeure", "Villa",
           "Habitat", "Appartement", "Residence", "Residentiel", "Bleu",
           "Magasin", "Boutique", "Moderne", "Minimaliste", "Design"]


def home(request):

    # Setup cookie for RGPD info

    rgpd = request.COOKIES.get('rgpd')

    date = Project.date.get_queryset()
    date = Project.date.get_queryset()[len(date)-1].date

    # define contextual variable
    context = contextualisor()

    # return template with contextual variable
    pprint(context)
    response = render(request, 'home/home.html', context)

    if rgpd is None:
        response.set_cookie('rgpd', 'display')

    return response


def home_selected(request, category, date=None):

    # Setup cookie for RGPD info

    print(category, date)

    rgpd = request.COOKIES.get('rgpd')

    # Set query by architecture and last year project

    category = category
    nav = True

    print(category, date)
    if category == 'Architecture' and date is None:
        date = Project.date.get_queryset()
        date = str(Project.date.get_queryset()[len(date)-1].date)

    print(date)
    # define contextual variable

    context = contextualisor(category, date, rgpd, nav)

    # return template with contextual variable

    response = render(request, 'home/home.html', context)

    if rgpd is None:
        response.set_cookie('rgpd', 'display')

    # print(category, date, type(date))
    # pprint(context)

    return response


def contextualisor(category=None, date=None, rgpd=None, nav=None):

    project = firstProjectPicture(category=category, date=date)

    context = {'years': [str(i.date) for i in Project.date.get_queryset()],
               'selected': {'category': category,
                            'date': date},
               'alt': altList,
               'rgpd': rgpd,
               'nav':  nav,
               }

    list_project_picture = []

    for p in project:
        home_pictures = {}

        if len(p.image_set.get_queryset()) != 0:
            home_pictures["url"] = p.image_set.get_queryset()[0].image.url
        else:
            home_pictures["url"] = "/static/logo/img_not_loaded.png"
        home_pictures["tag"] = random.choice(altList)
        home_pictures["category"] = p.category
        home_pictures["date"] = p.date
        home_pictures["project"] = p.name

        list_project_picture.append(home_pictures)

    context['home_pictures'] = list_project_picture

    return context


def firstProjectPicture(category=None, date=None):
    """return first picture and data of selected project"""

    if date is None and category is None:
        project = get_list_or_404(Project.objects.all().order_by("-date"))

    # get all project by category and year
    elif date is None:
        project = get_list_or_404(Project.objects.all().order_by("-date"),
                                  category__category=category)

    else:
        project = get_list_or_404(Project,
                                  category__category=category,
                                  date__date=date)

    # get first pics of project
    firstPicOfProject = []
    projectIn = []
    for p in project:
        if p.name not in projectIn:
            projectIn.append(p.name)
            firstPicOfProject.append(p)

    return firstPicOfProject


# AJAX requests
def get_project(request):
    """Return json file include project data for ajax managment for porojectViewer"""
    project = request.GET["project"].strip("/")
    project = Project.objects.filter(name=project)[0]
    projectImage = Project.objects.filter(name=project)[0].image_set.get_queryset()

    data = {"name": project.name,
            "text": project.text.replace('\n', '<br>'),
            "category": project.category.category,
            "date": project.date.date,
            "feature": {},
            "pictures": {}}

    if len(projectImage) != 0:
        for image in projectImage:
            data["pictures"][image.number] = {"imageUrl": image.image.url,
                                              "thumbnailUrl": image.miniature.url,
                                              "number": image.number,
                                              "caption": image.caption,
                                              "credit": image.credit,
                                              "alt": random.choice(altList)}

    else:
        data["pictures"][0] = {"imageUrl": "/static/logo/img_not_loaded.png",
                               "thumbnailUrl": "/static/logo/img_not_loaded.png",
                               "number": 1,
                               "caption": "",
                               "credit": "",
                               "alt": random.choice(altList)}

    data['feature'] = {}

    if project.category.category == "Design":
        if project.designFeature:
            if project.designFeature.measure:
                data["feature"]["dimensions"] = project.designFeature.measure
            if project.designFeature.date:
                data["feature"]["date de creation"] = project.designFeature.date
            if project.designFeature.material:
                data["feature"]["materiaux"] = project.designFeature.material

    elif project.category.category == "Architecture":
        if project.architectureFeature:
            if project.architectureFeature.date:
                data["feature"]["date"] = project.architectureFeature.date
            if project.architectureFeature.mission:
                data["feature"]["mission"] = project.architectureFeature.mission
            if project.architectureFeature.size:
                data["feature"]["superficie"] = project.architectureFeature.size
            if project.architectureFeature.statut:
                data["feature"]["statut"] = project.architectureFeature.statut

    return JsonResponse(data)


def menu_request(request):
    """Return json file include project data for ajax managment"""
    category = request.GET["category"].strip("/") or "Architecture"
    date = request.GET["date"].strip("/")

    if category == "Architecture":
        project = Project.objects.filter(category__category=category,
                                       date__date=date)
        # get first pics of project
        project = firstProjectPicture(category=category, date=date)
    else:
        project = Project.objects.filter(category__category=category)
        # get first pics of project
        project = firstProjectPicture(category=category)

    data = {}

    for y, i in enumerate(project, 0):
        data[y] = {}
        try:
            data[y]["url"] = i.image_set.get_queryset()[0].image.url
        except IndexError:
            data[y]["url"] = "/static/logo/img_not_loaded.png"
        data[y]["tag"] =  random.choice(altList)
        data[y]["category"] = i.category.category
        data[y]["project"] = i.name

    return JsonResponse(data)


def page_not_found(request, *args, **argv):
    response = render(request,
                      'error/error404.html',
                      )
    response.status_code = 404

    return response


def acces_denied(request, *args, **argv):
    response = render(request,
                      'error/error500.html'
                      )

    response.status_code = 500

    return response
