from django.shortcuts import render
from home.models import Project


def mission(request):

    context = {'menu': 'studio',
               'page': 'mission',
               'years': [i.date for i in Project.date.get_queryset()]}

    return render(request, 'mission/mission.html', context)
