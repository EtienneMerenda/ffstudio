from django.contrib import admin
from home.admin import myAdminSite
from .models import Publication, Information, WebAdress


class PublicationAdmin(admin.ModelAdmin):

    # Display field main page
    list_display = ('__str__',
                    'number',
                    'image_number',
                    )

    # Display in publication set
    fields = ('publisher',
              'title',
              'number',
              'image',
              'image_display',
              'caption',
              'webAdress',
              'info',)

    readonly_fields = ('image_display', 'image_number')


class WebAdressAdmin(admin.ModelAdmin):

    list_display = ('__str__',
                    'id',)

    fields = ('adress',
              'all_adress')

    readonly_fields = ('all_adress',)


class InformationAdmin(admin.ModelAdmin):
    list_display = ('__str__',
                    'id',)

    fields = ('info',
              'all_info')

    readonly_fields = ('all_info',)


myAdminSite.register(Publication, PublicationAdmin)
myAdminSite.register(Information, InformationAdmin)
myAdminSite.register(WebAdress, WebAdressAdmin)
