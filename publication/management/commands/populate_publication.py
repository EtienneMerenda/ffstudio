from django.core.management.base import BaseCommand
import json
from pprint import pprint
from publication.models import Publication, WebAdress, Information

class Command(BaseCommand):
    args = "<à quoi sers tu ?>"
    help = "Fonction qui update les data d'un json pour remplir la db associée."

    def _populate_publications(self):

        with open("./publication/management/commands/publication.json", "r", encoding="utf-8") as file:
            publi = json.load(file)
        pprint(publi)

        Publication.objects.all().delete()
        WebAdress.objects.all().delete()
        Information.objects.all().delete()

        for title, data in publi.items():
            print(title)
            print(data)

            pu = Publication()
            pu.title = title

            pu.number = data['number']
            pu.caption = data['credit']
            pu.save()

            if data['info']:
                for value in data['info']:
                    print('info: ', value)
                    _info, created = Information.objects.get_or_create(info=value)
                    _info.save()
                    pu.otherInfo.add(_info)

            if data['webAdress']:
                for value in data['webAdress']:
                    adr, created = WebAdress.objects.get_or_create(adress=value)
                    adr.save()
                    pu.webAdress.add(adr)

            pu.save()

    def handle(self, *args, **options):
        self._populate_publications()
