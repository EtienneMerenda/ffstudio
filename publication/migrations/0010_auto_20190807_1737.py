# Generated by Django 2.2.2 on 2019-08-07 15:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('publication', '0009_auto_20190807_1644'),
    ]

    operations = [
        migrations.RenameField(
            model_name='publication',
            old_name='otherInfo',
            new_name='info',
        ),
    ]
