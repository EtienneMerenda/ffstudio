# Generated by Django 2.2.5 on 2020-01-29 14:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publication', '0015_auto_20190808_0859'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='information',
            options={'verbose_name': 'Information', 'verbose_name_plural': 'Informations'},
        ),
        migrations.AlterModelOptions(
            name='webadress',
            options={'verbose_name': 'Lien', 'verbose_name_plural': 'Liens'},
        ),
        migrations.AddField(
            model_name='publication',
            name='publieur',
            field=models.CharField(blank=True, max_length=200, verbose_name='Publié par:'),
        ),
        migrations.AlterField(
            model_name='information',
            name='info',
            field=models.CharField(blank=True, max_length=200, unique=True, verbose_name='Information à lier à une publication'),
        ),
        migrations.AlterField(
            model_name='publication',
            name='number',
            field=models.SmallIntegerField(unique=True, verbose_name='Numéro de la publication'),
        ),
        migrations.AlterField(
            model_name='publication',
            name='title',
            field=models.CharField(max_length=200, verbose_name='Titre de la publication'),
        ),
        migrations.AlterField(
            model_name='webadress',
            name='adress',
            field=models.CharField(blank=True, max_length=200, unique=True, verbose_name='Lien'),
        ),
    ]
