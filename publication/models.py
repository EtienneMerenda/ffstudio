from django.db import models
from django.utils.safestring import mark_safe
from django.utils.html import format_html, format_html_join


class Information(models.Model):
    info = models.CharField(max_length=200, blank=True, unique=True,
                            verbose_name='Information à lier à une publication')

    def __str__(self):
        return self.info

    def all_info(self):

        return format_html_join('\n', '<li>{}</li>',
                           ((i.info,) for i in Information.objects.all()))
    all_info.short_description = 'Infos enregistrés'

    class Meta:
        verbose_name = 'Information'
        verbose_name_plural = 'Informations'



class WebAdress(models.Model):
    adress = models.CharField(max_length=200, blank=True, unique=True,
                              verbose_name='Lien')

    def __str__(self):
        return self.adress

    def all_adress(self):

        return format_html_join('\n', '<li>{}</li>',
                           ((a.adress,) for a in WebAdress.objects.all()))
    all_adress.short_description = 'Liens enregistrés'

    class Meta:
        verbose_name = 'Lien'
        verbose_name_plural = 'Liens'


class Publication(models.Model):
    title = models.CharField(max_length=200,
                             verbose_name='Titre de la publication'
                             )
    publisher = models.CharField(max_length=200,
                                verbose_name='Publié par:',
                                blank=True
                                )
    number = models.SmallIntegerField(unique=True, verbose_name='Numéro de la publication')
    image = models.ImageField(upload_to='pictures/Publication', blank=True, null=True,
                              verbose_name='Image de la publication')

    caption = models.CharField(max_length=200, blank=True, null=True,
                               verbose_name='Légende et crédit')
    info = models.ManyToManyField(Information,
                                  blank=True,
                                  verbose_name="Information annexe")
    webAdress = models.ManyToManyField(WebAdress,
                                       blank=True,
                                       verbose_name="Lien lié")

    def image_display(self):
        try:
            img = format_html('<img src="{}" style="width: auto; height: auto;'
                              'max-width: 200px; max-height: 200px">',
                              self.image.url)
            return img

        except ValueError:
            noImg = format_html('<span>{}</span>',
                                'Aucune image.')
            return noImg

    image_display.short_description = "Image"

    def image_number(self):
        try:
            print(self.image.url)
            number = format_html('<span>{}</span>',
                                 len((self.image.url,)))

            return number

        except ValueError:
            noImg = format_html('<span>{}</span>',
                                'Aucune image.')
            return noImg

    image_number.short_description = "Nombre d'image"

    def __str__(self):
        return self.title

    class Meta:
        ordering = ["number"]
