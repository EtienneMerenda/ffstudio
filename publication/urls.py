from django.urls import re_path
from publication import views

urlpatterns = [
    re_path(r'^publication/$', views.publication),
]
