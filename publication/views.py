from django.shortcuts import render
from .models import Publication
from home.models import Project
from pprint import pprint


def publication(request):

    publications = Publication.objects.all().order_by('-number')
    context = {'page': 'publication',
               'menu': 'studio',
               'years': [i.date for i in Project.date.get_queryset()],
               'publications': []}
    for publication in publications:
        publication_ = {}
        publication_['title'] = publication.title
        if publication.image:
            publication_['imageUrl'] = publication.image.url
        publication_['caption'] = publication.caption
        publication_['info'] = [i.info for i in publication.info.all()]
        publication_['webAdress'] = [i.adress for i in publication.webAdress.all()]
        publication_['publisher'] = publication.publisher
        context['publications'].append(publication_)

    return render(request, 'publication/publication.html', context)
