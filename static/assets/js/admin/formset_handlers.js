// script used for manage displaying row in relation with category selected

var category = document.querySelector('#id_category')

if (category !== null){

    console.log('Script used to display or hide rows added');

// rows selected to display or not
var architectureFeature = document.getElementsByClassName('form-row field-architectureFeature')[0]
var designFeature = document.getElementsByClassName('form-row field-designFeature')[0]

var archiValue = document.querySelector('#id_architectureFeature')
var designValue = document.querySelector('#id_designFeature')

function displayGoodOne(){
    var selectedCategory = category.options[category.selectedIndex].innerText
    if (selectedCategory === 'Architecture') {
        // set design unselected and don't display
        designFeature.style.display = 'None'
        //designValue.value = ''
        // display architecture row
        architectureFeature.style.display = 'block'

    } else {
        architectureFeature.style.display = 'None'
        //archiValue.value = ''
        designFeature.style.display = 'block'
    }
}


displayGoodOne()

// Add listener in switch category.
category.addEventListener('click', function(){
    displayGoodOne()
})

}else{console.log('category script not added. (Used for Project admin)');}
