const mail_form = document.getElementsByClassName('email')[0]

const sending = document.getElementsByClassName('sending')[0]
const form = document.getElementsByClassName('email')[0]
// console.log(mail_form);

mail_form.addEventListener("submit", function(e) {
    const response = grecaptcha.getResponse();
    if (response.length == 0) {
        e.preventDefault();

        if (document.getElementsByClassName('recaptcha_failed').length === 0) {
            let span = document.createElement('span');
            span.className = 'recaptcha_failed';
            span.innerHTML = "Validez le REcaptcha avant l'envoi.";
            form.insertBefore(span, sending);
        }
        return false;
    }
})
