// ------------------------------- desktop menu effects -------------------------------


// Generate the gallery with ajax data
function galleryMaker(data) {
    $('.gallery').fadeOut(100, function() {
      $('.gallery .content').empty()
      $.each(data, function(i, image) {
        var img = $('<img />').attr('src', image.url);
        img.attr('class', 'projectPicture');
        img.attr('id', image.project)
        img.attr('alt', image.tag)
        img.attr('onload', "onloadOpacity(this)")
        var figcaption = $('<figcaption></figcaption>').text(image.project)
        var figure = $('<figure></figure>').append(img);
        figure.append(figcaption)
        $('.gallery .content').append(figure)
      })
    });
    $('.gallery').fadeIn(100);
}

// Function used for request FFStudio server.
function ajaxGallery() {
    var category = $('#focusDesktop').text();
    var date = $('#focused').text();
    $.ajax({
        url: '/ajax/category/date/',
        type: 'GET',
        data: {'category': category, 'date': date},
        dataType: 'json',
        success: function(data){/* console.log(data); */
                                galleryMaker(data)
                            },
        error: function(){console.log('ajax error')}
    })
}

// Change focus on all platform
function idFocus(choice) {
    if (choice === "d") {
        $('.DMenu').attr('id', 'focusDesktop');
        $('.AMenu').attr('id', 'unfocusDesktop');
        $('.DTopMenuMobile').attr('id', 'focusTop');
        $('.ATopMenuMobile').attr('id', 'unfocusTop');

    }else if (choice === "a"){
        $('.DMenu').attr('id', 'unfocusDesktop');
        $('.AMenu').attr('id', 'focusDesktop');
        $('.DTopMenuMobile').attr('id', 'unfocusTop');
        $('.ATopMenuMobile').attr('id', 'focusTop');
    }
}

// Project  ---  Studio ---  display sub menu and years menu

$(function(){
    // on studio click, deploy submenu else fadeOut it
    $('#studio').click(function() {
        // Remove year style when user change menu
        // $('.years').fadeOut(100, function() {$('.years').removeAttr('style');})

        if ($('#innerMenuStudio').css('display') !== "block"){
            $('#innerMenuStudio').fadeIn(100);
            $('#innerMenuProject').fadeOut(100);

            $('#studio').attr('class', 'select');
            $('#project').removeAttr('class');
            // improve .content size
            $('.menuDesktop > .content').attr('id', 'extendStudio');
        } else {
            $('#innerMenuStudio').fadeOut(100);
            $('#studio').removeAttr('class');
            // If all subMenu are not display, reduce size of menu
            if ($('#innerMenuProject').css('display') !== "block") {
                $('.menuDesktop > .content').removeAttr('id');
            }
        }
    })

    $('#project').click(function() {
        // on project click, deploy submenu else fadeOut it
        if ($('#innerMenuProject').css('display') !== "block"){
            $('#innerMenuProject').fadeIn(100);
            $('#innerMenuStudio').fadeOut(100);

            $('#studio').removeAttr('class');
            $('#project').attr('class', 'select');

            // improve .content size
            $('.menuDesktop > .content').attr('id', 'extendProject');
        } else {
            // Don't hide years menu on closed
            // $('.years').fadeOut(100, function() {$('.years').removeAttr('style');})
            $('#innerMenuProject').fadeOut(100);
            $('#project').removeAttr('class');
            // If all subMenu are not display, reduce size of menu
            if ($('#innerMenuStudio').css('display') !== "block") {
                $('.menuDesktop > .content').removeAttr('id');
            }
        }
    })

    // Add auto close menu if mouse leave menu
    $('.menuDesktop').on("mouseleave", function() {
        // console.log("mouseleave");
        if ($('#innerMenuProject').css('display') == "block") {
            $('#project').click()
        } else if ($('#innerMenuStudio').css('display') == "block") {
            $('#studio').click()
        }
    })

    // Set position of years div when user click
    $('.AMenu').click(function() {
        idFocus("a");
        if ($('.years').parent().attr('class') !== 'yearsDesktop') {
            $('.years').css('display', 'block')
            $('.years').appendTo($('.yearsDesktop'));
            $('.years').fadeTo(100, 1, initialiseScroll()); //script in scrollTo.js
        };
        if ($('.years').css('display') !== 'block') {
            $('.years').css('display', 'block')
            $('.years').fadeTo(100, 1, initialiseScroll()); //script in scrollTo.js
        };

    })

    $('.DMenu').click(function() {

        if ($('.DMenu').attr('id') !== "focusDesktop"){
            idFocus("d")
            $('.years').fadeTo(150, -0.5, function() {
                $('.years').removeAttr('style');
                $('.year').attr('id', 'unfocused')
                $('.X').click()
                window.scrollTo({top: 0,
                                left: 0,
                                behavior: 'smooth'});
            });
            ajaxGallery();
        }
    })
})

// ------------------------------- mobile menu effects -------------------------------

//  burgerMenu --- Set display bock and fadeIn whhen user click on.


$(function() {
    $('.burgerMenu').click(function() {
        if ($('.menuMobile').css('display') === 'none') {
            // open menu
            $('.menuMobile').css('display', 'block');
            $('.menuMobile .content').fadeTo(100, 1);

            // years
            $('.years').attr('id', 'mobileMenuYears')

            $('.years').css('display', 'none')
            $('.menuMobile').append($('.years'))
        } else {
            $('.yearsMenuMobile').fadeTo(200, -1)
            $('.menuMobile > .content').fadeTo(200, -1, function() {
                $('.menuMobile').removeAttr('style');
                $('.years').removeAttr('style');
                if ($('#focusTop').text() === 'Design') {
                    // console.log('FadeOut');
                    $('.years').fadeTo(0, 0)
                } else {
                    // console.log('FadeIN');
                    $('.years').fadeIn(100, initialiseScroll());
                    window.scrollTo({top: 0,
                                    left: 0});
                };
                $('.topMenu').append($('.years'));

                $('.years').attr('id', 'mobileTopMenuYears')

            });
        }
    })

    // yearsMenu --- postion and size in menuPanel

    $('#AMenuMobile').on("click", function() {
        // Get name of category
        $('.ATopMenuMobile').click();

        $actual_cat = $('.categoryHeader[style]').text();
        $actual_year = $('.yearHeader[style]').text();
        $menu_category = $(this).text();

        // Apply focus style on years
        $('.yearMenu:contains($actual_year)').css('font-size', '21px').css('font-weight', '300');
        // console.log($menu_category);
        // console.log($actual_cat);
        // console.log($actual_year);
        // get position of clicked element
        var position = $(this).offset();
        var moved_position = $(window).scrollTop();
        position = {'top': position['top'] - moved_position -6, left: '10vw'};
        $('.years').css('position', 'fixed')
        $('.years').css('display', 'inline-flex').fadeTo(100, 1, initialiseScroll())
        $('.years').css(position);

    })

    $('#DMenuMobile').on("click", function() {

        $('.DTopMenuMobile').click();
        $('.burgerMenu').click();
        $('.X').click();
    })
})

// Setup year click
$(function() {
    $('.year').on('click', function() {

        // console.log($(this).attr('id'));

        if ($(this).attr('id') !== 'focused' && $('.menuMobile').css('display') !== 'block') {

            $('#focused').attr('id', 'unfocused');
            $(this).attr('id', 'focused');
            window.scrollTo({top: 0,
                            left: 0,
                            behavior: 'smooth'});

            ajaxGallery()

        } else if ($('.menuMobile').css('display') === 'block') {
             if ($(this).attr('id') === 'focused') {
                 $('.burgerMenu').click();
             } else {
                 $('#focused').attr('id', 'unfocused');
                 $(this).attr('id', 'focused');

                 ajaxGallery()
                 $('.burgerMenu').click();
             }
        } if ($('.projectViewer').css('display') === "block" ) {
            $('.X').click();
            $(this).attr('id', 'focused');
        }
    })
});

$(function() {
    $('.ATopMenuMobile').on('click', function() {

        if ($(this).attr('id') !== 'focusTop') {
            // console.log($(this).attr('class'));

            $('.years').fadeTo(100, 1, initialiseScroll());

            idFocus("a");

        }
    })

    $('.DTopMenuMobile').on('click', function() {
        if ($(this).attr('id') !== 'focusTop') {
            // console.log($(this).attr('class'));

            $('.years').fadeOut(150);
            $('.year').attr('id', 'unfocused');
            idFocus("d");

            // get files in directory:
            ajaxGallery();
        }
    })
});

(function() {
    $('#designMenu').on('click', function() {

    })
})

// Add EventListener in chevron if mare than 3 years

var leftArrow = document.querySelector('.leftChevron');
var rightArrow = document.querySelector('.rightChevron');
var sliderInner = document.querySelector('.slider')
var slider = document.querySelector('.years')

if (leftArrow !== null && leftArrow !== undefined) {

  var updatePosition

  leftArrow.addEventListener('click', function() {
    viewLarger = slider.getBoundingClientRect().width
    slider.scrollBy({ left: -viewLarger, behavior: 'smooth' });
  });
  rightArrow.addEventListener('click', function() {
    viewLarger = slider.getBoundingClientRect().width
    slider.scrollBy({ left: viewLarger, behavior: 'smooth' });
  });
}

// Setup exit when user click on X icon for Project_viewer ---------------------

$('.X').on("click", function() {
  $('.projectViewer').fadeTo(175, -0.5, function(){
      $('.gallery').removeAttr('style');
      $('.topMenu').removeAttr('style');
      $('.projectViewer').removeAttr('style');
  });
  // $(window).scrollTop($screenPosition);
})
