// Script used for scroll in year's slider

if (document.querySelector('.sliderView') !== null) {
    // console.log('slider created, function initialiseScroll created');

    function initialiseScroll(){
        var slider = document.querySelector('.sliderView');
        var width = slider.getBoundingClientRect().width;
        // console.log(slider);
        // console.log(width);
        slider.scroll({top: 0,
                      left: slider.scrollLeft + width,
                      behavior: 'smooth'});
        // console.log('Position setup');
    };
    } else {
    console.log('Slider not created. Not enough years.');
    function initialiseScroll() {};
}
