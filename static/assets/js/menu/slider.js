// Used for change size or weight.

function upOne(element, size, weight) {
  if (size !== undefined) {
    element.style.fontSize = size;
  };
  if (weight !== undefined) {
    element.style.weight = weight;
  };
};

// Add EventListener in chevron if mare than 3 years

var leftArrow = document.querySelector('.leftChevron');
var rightArrow = document.querySelector('.rightChevron');
var slider = document.querySelector('.sliderView');

if (leftArrow !== null && leftArrow !== undefined) {

  leftArrow.addEventListener('click', function() {
    // console.log('left')
    var viewLarger = slider.getBoundingClientRect().width
    // slider.scrollLeft -= viewLarger
    slider.scroll({top: 0,
                  left: slider.scrollLeft - viewLarger,
                  behavior: 'smooth'});
  });
  rightArrow.addEventListener('click', function() {
    // console.log('right')
    var viewLarger = slider.getBoundingClientRect().width
    // slider.scrollLeft += viewLarger
    slider.scroll({top: 0,
                  left: slider.scrollLeft + viewLarger,
                  behavior: 'smooth'})
  });
}
