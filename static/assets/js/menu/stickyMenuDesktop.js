// Make menuDesktop sticky -------------------------------------------------------------

// When the user scrolls the page, execute myFunction 
window.onscroll = function() {myFunction()};

// Get the header
var menu = document.querySelector(".menuDesktop");

// Get the offset position of the navbar
var sticky = menu.offsetTop;

// Add the sticky position to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
    if (window.pageYOffset > sticky && window.innerWidth > 479) {
        menu.style.position = "sticky";
        menu.style.top = "0px";
    } else {
        menu.removeAttribute("style");
    }
}


