// Set id of menu according to window width

// init id for years menu and years position
$(function() {
    if (window.innerWidth < 480) {
        $('.years').attr('id', 'mobileTopMenuYears');
        $('.topMenu').append($('.years'))
        if ($('#focusTop').text() === 'Design'){
            $('.years').css('display', 'none');
        }
    } else {
        $('.years').attr('id', 'desktopMenuYears');
        $('.appendYearsDesktop').append($('.years'))
        // console.log($('.appendYearsDesktop'));
        // console.log($('.years'));
    }
    initialiseScroll()
})


$(window).resize(function() {
    if (window.innerWidth < 480) {
        $('.menuDesktop').css('display', 'none');
        // set years in topMenu according to width and id according to width
        if ($('.menuMobile').css('display') === 'none') {
            $('.topMenu').append($('.years'));
            $('.years').attr('id', 'mobileTopMenuYears');
            $('.years').removeAttr('style')
            initialiseScroll()
        }
        // $('.years').removeAttr('style');
    } else {
        $('.menuMobile').css('display', 'none');
        $('.menuDesktop').css('display', 'block');
        // set years id to home style according to width
        $('.years').attr('id', 'desktopMenuYears');
        if ($('.years').attr('id') !== 'desktopMenuYears') {
            $('.years').removeAttr('style')
        }
        if ($('.years').css('top') !== "9px") {
            $('.years').removeAttr('style');

        }
    }
})
