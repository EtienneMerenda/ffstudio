// Setup exit when user click on X icon ----------------------------------------

$('.X').click(function() {
  $('.gallery').removeAttr('style');
  $('.topMenu').removeAttr('style');
  $('.projectViewer').fadeTo(175, -0.5, function(){
    $('.projectViewer').removeAttr('style');
  });
  $(window).scrollTop($screenPosition);
})

$('body').on('click', '.X_picture', function () {
  $('.picture_viewer').fadeTo(175, -0.5, function(){
    $('.picture_viewer').removeAttr('style');
    $('header').removeAttr('style');
    $('.project_viewer').css('display', 'flex');
  });
});

// Create and manage picture_viewer --------------------------------------------

$('body').on('click', '.project_viewer img', (function() {
  $('.other_pictures').empty();
  $('.picture').empty();
  var pic_selected = $(this).attr('src');

  for (var url of pic_urls) {
    var img = $('<img />').attr('src', url);
    var img_pic = $('<img />').attr('src', url);
    var figure = $('<figure></figure>').append(img);
    var figure_pic = $('<figure></figure>').append(img_pic);

    if (url != pic_selected){
      $('.other_pictures').append(figure);
    } else {
      $('.picture').append(figure_pic);
      $('.other_pictures').append(figure);
    };

    $(window).scrollTop(0);
    $('header').css('display', 'none');
    $('.menu_haut').css('display', 'none');
    $('.project_viewer').css('display', 'None');
    $('.picture_viewer').css('z-index', '3');
    $('.picture_viewer').fadeTo(100, 1);

  };
}));


// Manage click on thumbnail

$('body').on('click', '.other_pictures img', function() {
  var url = $(this).attr('src');
  $('.picture img').fadeOut(100, function() {
    $('.picture img').attr('src', url);
  });
  $('.picture img').fadeIn(100);

});
