function switchImage (this_) {
    for (urlList of $imageList) {
        if ($(this_).attr('src') === urlList[1]) {
            var newFocus = urlList[0]
        } if ($('.projectViewer > .content  .project img').attr('src') === urlList[0])  {
            var newThumbnail = urlList[1]
        }
    }


    $('.projectViewer > .content  .project img').fadeTo(100, 0, function() {
        $('.projectViewer > .content  .project img').attr('src', newFocus);
        // $('.projectViewer > .content  .project img').fadeTo(100, 1);
    })
    $(this_).fadeTo(100, 0, function() {
        $(this_).attr('src', newThumbnail)
        // $(this_).fadeTo(100, 1);
    })
}

// Create project viewer

// Ajax request and build the viewer div
$(function() {
        $('body').on('click', '.projectPicture', function() {
        // get the name of project
        // console.log('projectViewer request');

        $imageList = [];
        var focusedProject = $(this).attr('id');
        $screenPosition = $(window).scrollTop();
        $.ajax({
          url: '/ajax/project/',
          type: 'GET',
          data: {'project': focusedProject},
          dataType: 'json',
          success: function(data){/* console.log('projectViewer: ', data); */

                                  // Setup the project_viewer div
                                  $('.projectViewer > .content  .project h2').text(data.name);
                                  $('.projectViewer > .content  .textDesktop h2').text(data.name);


                                  // Delete old pictures
                                  $('.projectViewer > .content .pictureSample').empty();
                                  // create thumbnail
                                  $.each(data["pictures"], function(i, image) {
                                      $imageList.push([image.imageUrl, image.thumbnailUrl]);
                                      // console.log(image);
                                    if (image.number === 1){
                                        $('.projectViewer > .content .project img').attr('src', image.imageUrl);
                                        $('.projectViewer > .content .project img').attr('alt', image.alt);
                                        // $('.projectViewer > .content .project img').attr('onclick', "window.open(this.src)")
                                        $('.projectViewer > .content .project figcaption').text(image.credit);
                                    } else {
                                      var img = $('<img />').attr('src', image.thumbnailUrl);
                                      img.attr('onload', "onloadOpacity(this)")
                                      var figure = $('<figure></figure>').append(img).attr('class','thumbnail');
                                      $('.pictureSample').append(figure);
                                    }
                                  });

                                  // Set size of screen and design link display
                                  if (data.category === 'Design') {
                                      // console.log('window width:', $(window).width());
                                      if ($(window).width() < 480) {
                                          $('.projectViewer .linkDesignMobile').css('display', 'block')
                                          $('.projectViewer .linkDesignDesktop').css('display', 'none')
                                      } else {
                                          $('.projectViewer .linkDesignMobile').css('display', 'none')
                                          $('.projectViewer .linkDesignDesktop').css('display', 'block')
                                      }
                                  } else {
                                      $('.projectViewer .linkDesignDesktop').css('display', 'none')
                                      $('.projectViewer .linkDesignMobile').css('display', 'none')
                                  }

                                  // Add feature
                                  $('.projectViewer > .content .feature').empty();
                                  $.each(data.feature, function(i, feature) {
                                      var div = $('<div></div>');
                                      var title = $('<h6></h6>').text(i.toUpperCase());
                                      var value = $('<p></p>').html(feature);
                                      $(div).append(title);
                                      $(div).append(value);
                                      $(".projectViewer > .content .feature").append(div);
                                  })
                                  //Add text
                                  $('.projectViewer > .content .textMobile > p').html(data.text);
                                  $('.projectViewer > .content .textDesktop > p').html(data.text);



                                  // Hide the main page and go to the top
                                  $(window).scrollTop(0);
                                  $('.gallery').css('display', 'None');
                                  $('.topMenu').css('display', 'None');

                                  // Show the project_viewer
                                  $('.projectViewer').css('z-index', '1')
                                  $('.projectViewer').fadeTo(100, 1);

                                },
          error: function(){console.log('ajax error');console.log(data);},
        });
    })

    $('body').on('click', '.thumbnail img', function() {
        // Reactivate background svg loading
        $('.project figure').css('background-image', '');
        switchImage(this);
    })

});
